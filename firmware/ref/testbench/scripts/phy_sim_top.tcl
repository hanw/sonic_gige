
set QUARTUS_ROOTDIR $env(QUARTUS_ROOTDIR)
set simulator_lib mentor

vlib work
vlib sv_generic

###########################################
# Stratix V library files 
###########################################
vlog -work sv_generic $QUARTUS_ROOTDIR/eda/sim_lib/altera_lnsim.sv
vlog -work sv_generic $QUARTUS_ROOTDIR/eda/sim_lib/stratixv_atoms.v
vlog -work sv_generic $QUARTUS_ROOTDIR/eda/sim_lib/stratixv_hssi_atoms.v
vlog -work sv_generic $QUARTUS_ROOTDIR/eda/sim_lib/mentor/stratixv_atoms_ncrypt.v
vlog -work sv_generic $QUARTUS_ROOTDIR/eda/sim_lib/mentor/stratixv_hssi_atoms_ncrypt.v
vlog -work sv_generic $QUARTUS_ROOTDIR/eda/sim_lib/220model.v
vlog -work sv_generic $QUARTUS_ROOTDIR/eda/sim_lib/altera_primitives.v
vlog -work sv_generic $QUARTUS_ROOTDIR/eda/sim_lib/sgate.v
vlog -work sv_generic $QUARTUS_ROOTDIR/eda/sim_lib/altera_mf.v

###########################################
# PHY Instance top_gige
###########################################
vlib msim_top_gige
vlog -work msim_top_gige -sv top_gige_sim/altera_xcvr_custom_phy/alt_xcvr_csr_common_h.sv
vlog -work msim_top_gige -sv top_gige_sim/altera_xcvr_custom_phy/alt_xcvr_csr_pcs8g_h.sv
vlog -work msim_top_gige -sv top_gige/altera_xcvr_functions.sv
vlog -work msim_top_gige top_gige_sim/altera_xcvr_custom_phy/altera_wait_generate.v
vlog -work msim_top_gige -sv top_gige_sim/altera_xcvr_custom_phy/altera_xcvr_custom.sv
vlog -work msim_top_gige -sv top_gige_sim/altera_xcvr_custom_phy/altera_xcvr_functions.sv
vlog -work msim_top_gige -sv top_gige_sim/altera_xcvr_custom_phy/alt_reset_ctrl_lego.sv
vlog -work msim_top_gige -sv top_gige_sim/altera_xcvr_custom_phy/alt_reset_ctrl_tgx_cdrauto.sv
vlog -work msim_top_gige -sv top_gige_sim/altera_xcvr_custom_phy/alt_xcvr_arbiter.sv
vlog -work msim_top_gige -sv top_gige_sim/altera_xcvr_custom_phy/alt_xcvr_csr_common.sv
vlog -work msim_top_gige -sv top_gige_sim/altera_xcvr_custom_phy/alt_xcvr_csr_common_h.sv
vlog -work msim_top_gige -sv top_gige_sim/altera_xcvr_custom_phy/alt_xcvr_csr_pcs8g.sv
vlog -work msim_top_gige -sv top_gige_sim/altera_xcvr_custom_phy/alt_xcvr_csr_pcs8g_h.sv
vlog -work msim_top_gige -sv top_gige_sim/altera_xcvr_custom_phy/alt_xcvr_csr_selector.sv
vlog -work msim_top_gige -sv top_gige_sim/altera_xcvr_custom_phy/alt_xcvr_m2s.sv
vlog -work msim_top_gige -sv top_gige_sim/altera_xcvr_custom_phy/alt_xcvr_mgmt2dec.sv
vlog -work msim_top_gige -sv top_gige_sim/altera_xcvr_custom_phy/alt_xcvr_resync.sv
vlog -work msim_top_gige -sv top_gige_sim/altera_xcvr_custom_phy/stratixv_hssi_10g_rx_pcs_rbc.sv
vlog -work msim_top_gige -sv top_gige_sim/altera_xcvr_custom_phy/stratixv_hssi_10g_tx_pcs_rbc.sv
vlog -work msim_top_gige -sv top_gige_sim/altera_xcvr_custom_phy/stratixv_hssi_8g_rx_pcs_rbc.sv
vlog -work msim_top_gige -sv top_gige_sim/altera_xcvr_custom_phy/stratixv_hssi_8g_tx_pcs_rbc.sv
vlog -work msim_top_gige -sv top_gige_sim/altera_xcvr_custom_phy/stratixv_hssi_common_pcs_pma_interface_rbc.sv
vlog -work msim_top_gige -sv top_gige_sim/altera_xcvr_custom_phy/stratixv_hssi_common_pld_pcs_interface_rbc.sv
vlog -work msim_top_gige -sv top_gige_sim/altera_xcvr_custom_phy/stratixv_hssi_pipe_gen1_2_rbc.sv
vlog -work msim_top_gige -sv top_gige_sim/altera_xcvr_custom_phy/stratixv_hssi_pipe_gen3_rbc.sv
vlog -work msim_top_gige -sv top_gige_sim/altera_xcvr_custom_phy/stratixv_hssi_rx_pcs_pma_interface_rbc.sv
vlog -work msim_top_gige -sv top_gige_sim/altera_xcvr_custom_phy/stratixv_hssi_rx_pld_pcs_interface_rbc.sv
vlog -work msim_top_gige -sv top_gige_sim/altera_xcvr_custom_phy/stratixv_hssi_tx_pcs_pma_interface_rbc.sv
vlog -work msim_top_gige -sv top_gige_sim/altera_xcvr_custom_phy/stratixv_hssi_tx_pld_pcs_interface_rbc.sv
vlog -work msim_top_gige -sv top_gige_sim/altera_xcvr_custom_phy/sv_pcs.sv
vlog -work msim_top_gige -sv top_gige_sim/altera_xcvr_custom_phy/sv_pcs_ch.sv
vlog -work msim_top_gige -sv top_gige_sim/altera_xcvr_custom_phy/sv_pma.sv
vlog -work msim_top_gige -sv top_gige_sim/altera_xcvr_custom_phy/sv_reconfig_bundle_merger.sv
vlog -work msim_top_gige -sv top_gige_sim/altera_xcvr_custom_phy/sv_reconfig_bundle_to_ip.sv
vlog -work msim_top_gige -sv top_gige_sim/altera_xcvr_custom_phy/sv_reconfig_bundle_to_xcvr.sv
vlog -work msim_top_gige -sv top_gige_sim/altera_xcvr_custom_phy/sv_rx_pma.sv
vlog -work msim_top_gige -sv top_gige_sim/altera_xcvr_custom_phy/sv_tx_pma.sv
vlog -work msim_top_gige -sv top_gige_sim/altera_xcvr_custom_phy/sv_tx_pma_ch.sv
vlog -work msim_top_gige -sv top_gige_sim/altera_xcvr_custom_phy/sv_xcvr_avmm.sv
vlog -work msim_top_gige -sv top_gige_sim/altera_xcvr_custom_phy/sv_xcvr_custom_native.sv
vlog -work msim_top_gige -sv top_gige_sim/altera_xcvr_custom_phy/sv_xcvr_custom_nr.sv
vlog -work msim_top_gige -sv top_gige_sim/altera_xcvr_custom_phy/sv_xcvr_data_adapter.sv
vlog -work msim_top_gige -sv top_gige_sim/altera_xcvr_custom_phy/sv_xcvr_native.sv
vlog -work msim_top_gige -sv top_gige_sim/altera_xcvr_custom_phy/sv_xcvr_plls.sv
vlog -work msim_top_gige ../source/top_gige.v

###########################################
# Reconfig Instance
###########################################

vlog -sv ../source/top_reconfig/alt_xcvr_reconfig_h.sv
vlog -sv ../source/top_reconfig/sv_xcvr_h.sv
vlog -sv ../source/top_reconfig/altera_xcvr_functions.sv
vlog -sv ../source/top_reconfig/*.sv
vlog ../source/top_reconfig.v



###########################################
## Add User Design Files
###########################################
vlog -sv ../source/mgmt_commands_h.sv
vlog -sv ../source/mgmt_cpu.sv
vlog -sv ../source/mgmt_functions_h.sv
vlog -sv ../source/mgmt_memory_map.sv
vlog -sv ../source/mgmt_program.sv
vlog -sv ../source/mgmt_master.sv
vlog gige_datagen.v
vlog top.v
vlog top_gige.v
vlog top_tb.v

###########################################
## Top-level testbench 
###########################################
vlog ../source/top_tb.v

###########################################
# Invoke simulator 
###########################################
 # vsim -c -novopt -t 1ps $tb_name 
#vsim -c -t 1ps top_tb
vsim  -c -novopt -t 1ps top_tst -L sv_generic -L msim_top_gige

