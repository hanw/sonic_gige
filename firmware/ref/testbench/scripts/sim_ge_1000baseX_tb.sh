#!/bin/bash

TEST_NAME="ge_1000baseX_tb"

if [[ $1 != "" ]]; then
  TEST_NAME=$1
fi

LIBS="-L work -L ge_1000baseX_lib -L ge_1000baseX_tb"

TB="ge_1000baseX_tb.ge_1000baseX_tb"

vsim -gui -permit_unmatched_virtual_intf -novopt $TB -Gtest_name=\"$TEST_NAME\" $LIBS &
